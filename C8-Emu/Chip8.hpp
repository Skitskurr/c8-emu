#pragma once
#include "GenericHeader.hpp"
#include <vector> // thanks for being dogshit <stack>
#include <array>
#include "Chip8UI.hpp"
#undef max // gay shit thx.
#include <ctime>
#include <map>

namespace Chip8 {
	class Chip8 {
		//Really good reference for the ops https://github.com/trapexit/chipce8/blob/master/CHIP-8.md

		char memory[ 64 * 32 ] = {

		}; // wAW 4k

		static constexpr int rom_start = 512; // 0x200 in because interpreter takes up first 512 bytes
		/*Our tick units are actually in Khz. So 0.5 Khz == 500Hz, 0.06 Khz == 60hz */
		static constexpr float timerspeed = .06f; // Sound & Delay timers decrement every 60Hz tick.
		float clockspeed = 5.f; //fukye.

		int clockTicks = 0;
		int timerTicks = 0;


		std::clock_t timerClock = 0;
		std::clock_t cpuClock = 0;
		bool romLoaded = false;
		/*
			sprites are 8 wide 1->15 high. Set pixels flip colour at that screen pixel, otherwise black
			Each row of 8 pixels is read as bit-coded starting from memory location I; I value doesn�t change after the execution of this instruction.
			Carry flag set to 1 if any pixel flips from set to unset when drawn, otherwise 0 -- used for collision detection  ( wikipedia )
		*/
		char display[ 64 * 32 ] = { 0 }; //Waw 2048 monochromatic pixels
		bool drawFlag;

		struct C8Registers { // https://www.pdc.kth.se/~lfo/chip8/CHIP8.htm
			/*Data*/
			union {
				uint8_t ALL[ 16 ];
				struct {
					uint8_t V0;
					uint8_t V1;
					uint8_t V2;
					uint8_t V3;
					uint8_t V4;
					uint8_t V5;
					uint8_t V6;
					uint8_t V7;
					uint8_t V8;
					uint8_t V9;
					uint8_t VA;
					uint8_t VB;
					uint8_t VC;
					uint8_t VD;
					uint8_t VE;
					uint8_t VF;
				};
			};
			// flags bruh
			/*Instruction Ptr*/
			uint16_t ip : 12;
			uint16_t someirrelevantshit : 4;
			/*Address register*/
			uint16_t spritesheet; // commonly called I
			/* Timers */ // wot
			uint8_t delay;
			uint8_t sound;
			/* Stack */
			std::vector<uint16_t> stack; // 16 deep apparently.
		};
		Chip8UI* currentUI = 0;

	public:
		Chip8( );
		C8Registers registers;
		char * GetMemory( );
		uint16_t GetInstruction( ); // pC8->GetMemory( )[ pC8->registers.ip ];
		bool SetPixel( int x, int y, int height ); //We can make it return wether the pixel is flipped or not
		char * GetDisplay( );
		void UpdateScreen( ); // Once we figure out what we'll use to handle the graphics
		void SetCurrentUI( Chip8UI* ui );
		void tick( );
		bool LoadRom( const char* rom );
		void SetSkip( bool skip );
		void SetTick( float tickrate );
		bool IsRomLoaded( );
		bool skipNext;
		std::map<int, int> KeyMap;
	};

	namespace Instructions {

		struct Instruction {
			union {
				uint16_t opcode;
				struct {
					uint8_t lower;
					uint8_t upper; // topmost 8 bits plez
				} bytes;
			};
			using fn = void( *)( Chip8 * pC8, uint16_t opcode, uint16_t ins );
			fn effect;
			bool full = false; // read opcode as a full short or split it

			bool operator==( uint16_t instruction )
			{
				if ( full )
					return instruction == opcode;
				else
				{
					// first, make sure highest bits match, so we dont get any confusion like 0x500 & 0x100 being the right match.
					auto hInstr = instruction & 0xF000;
					auto hOp = opcode & 0xF000;

					if ( hOp != hInstr )
						return false;

					/*Because the Fs && Es && 8s dont follow tihs easy trend*/
					if ( hOp == 0xF000 || hOp == 0xE000 )
					{
						if ( ( opcode & 0xFF ) != ( instruction & 0xFF ) )
							return false;
					}
					else if ( hOp == 0x8000 ) // 8s always have 2 params with last 4 bits constant.
					{
						if ( ( opcode & 0xF ) != ( instruction & 0xF ) )
							return false;
					}

					return ( instruction & opcode ) == opcode;
				}
			}
		};
#define FN []( Chip8 * pC8, uint16_t opcode, uint16_t ins ) -> void

		//info from https://en.wikipedia.org/wiki/CHIP-8
		static Instruction instruction_set[] =
		{
			//	{ // 0NNN // not finding this rca program, plus this fucks up the operator== and i dont wanna fix.
			//		0, FN
			//{
			//	// CALL RCA 1802 < dont think this is used >
			//}
			//	},

			{
				0xE0, FN
				{
					auto display = pC8->GetDisplay( );
					//for ( int n = 0; n < 2048; n++ ) { display[ n ] = 0; } what lol
					memset( display, 0, 32 * 64 );
				}, true
			},

			{
				0xEE, FN
				{
					//return from subroutine
					auto ret = pC8->registers.stack.back( );
					pC8->registers.stack.pop_back( );// stl why not return the value on pop wtf
					pC8->registers.ip = ret;
				}, true
			},

			{ // 1NNN
				0x1000, FN
				{
					// GOTO NNN;
					auto addr = ins;
					addr &= ~( opcode );
					pC8->registers.ip = addr; // ? maybe. idk. lol.
				}
			},

			{ // 2NNN
				0x2000,  FN
				{
					// CALL *ADDR
					auto addr = ins;
					addr &= ~( opcode );
					pC8->registers.stack.push_back( pC8->registers.ip ); // ip already incremented.
					pC8->registers.ip = addr;
				}
			},

			{// 3XNN
				0x3000, FN
				{
					//SKIP NEXT INSTR IF VX == NN
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto nn = ( ( uint16_t )( ins << 8 ) ) >> 8; // lol 
					if ( pC8->registers.ALL[ reg ] == nn )
						pC8->SetSkip( true );
				}
			},

			{ // 4XNN
				0x4000, FN
				{
					// Skip if not equal
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto nn = ( ( uint16_t )( ins << 8 ) ) >> 8; // lol 
					if ( pC8->registers.ALL[ reg ] != nn )
						pC8->SetSkip( true );
				}
			},

			{ // 5XY0
				0x5000, FN
				{
					// Skip if vx == vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					if ( pC8->registers.ALL[ reg ] == pC8->registers.ALL[ reg2 ] )
						pC8->SetSkip( true );
				}
			},

			{ // 6XNN
				0x6000, FN
				{
					// set vx to nn
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto nn = ( ( uint16_t )( ins << 8 ) ) >> 8; // lol 
					pC8->registers.ALL[ reg ] = nn;
				}
			},

			{ // 7XNN
				0x7000, FN
				{
					// add nn to vx ( no CF change )
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto nn = ( ( uint16_t )( ins << 8 ) ) >> 8; // lol 
					pC8->registers.ALL[ reg ] += nn;
				}
			},

			{ // 8XY0
				0x8000, FN
				{
					// vx = vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					pC8->registers.ALL[ reg ] = pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY1
				0x8001, FN
				{
					// vx |= vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					pC8->registers.ALL[ reg ] |= pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY2
				0x8002, FN
				{
					// vx &= vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					pC8->registers.ALL[ reg ] &= pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY3
				0x8003, FN
				{
					// vx ^= vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					pC8->registers.ALL[ reg ] ^= pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY4
				0x8004, FN
				{
					// vx += vy. Sets VF to 1 if we carry, otherwise 0

					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 

					auto carry = ( ( uintptr_t )pC8->registers.ALL[ reg ] + ( uintptr_t )pC8->registers.ALL[ reg2 ] > std::numeric_limits<uint8_t>::max( ) );
					pC8->registers.VF = carry;

					pC8->registers.ALL[ reg ] += pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY5
				0x8005, FN
				{
					// vx -= vy. Sets VF to 0 if we borrow from msb, otherwise 1

					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 

					auto borrow = ( pC8->registers.ALL[ reg2 ] < pC8->registers.ALL[ reg ] );
					pC8->registers.VF = borrow;

					pC8->registers.ALL[ reg ] -= pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 8XY6
				0x8006, FN
				{
					// vx = ( vy >>= 1 ). Sets VF to LSB of VY before shift
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 

					auto carry = ( pC8->registers.ALL[ reg2 ] & 1 );
					pC8->registers.VF = carry;

					pC8->registers.ALL[ reg2 ] >>= 1; // divide by 2 lul.
					pC8->registers.ALL[ reg ] = pC8->registers.ALL[ reg2 ];
				}
			},

					/*Someone check my logic here thx.*/
			{ // 8XY7
				0x8007, FN
				{
					// vx = ( vy - vx ) Sets VF to 0 if borrow else 1
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 

					auto carry = !( pC8->registers.ALL[ reg ] > pC8->registers.ALL[ reg2 ] );
					pC8->registers.VF = carry;
					pC8->registers.ALL[ reg ] = pC8->registers.ALL[ reg2 ] - pC8->registers.ALL[ reg ];
				}
			},

			{ // 8XYE
				0x800E, FN
				{
					// vx = ( vy <<= 1 ). Sets VF to MSB of VY before shift
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 

					auto carry = ( pC8->registers.ALL[ reg2 ] & 0x8000 /*MSB*/ );
					pC8->registers.VF = carry;

					pC8->registers.ALL[ reg2 ] <<= 1; // divide by 2 lul.
					pC8->registers.ALL[ reg ] = pC8->registers.ALL[ reg2 ];
				}
			},

			{ // 9XY0
				0x9000, FN
				{
					// skip if vx != vy
					auto reg = ins & ~( opcode );
					reg >>= 8; // drop the NN to get the register.
					auto reg2 = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xF; // lol 
					if ( pC8->registers.ALL[ reg ] != pC8->registers.ALL[ reg2 ] )
						pC8->SetSkip( true );
				}
			},

			{ // ANNN
				0xA000, FN
				{
					// set I to NNN
					auto val = ins & ~( opcode );
					pC8->registers.spritesheet = val;
				}
			},

			{ // BNNN
				0xB000, FN
				{
					// Jump to NNN + V0
					auto val = ins & ~( opcode );
					pC8->registers.ip = pC8->registers.V0 + val;
				}
			},

			{ // CXNN 
				0xC000, FN
				{
					// Vx = rand() & NN
					auto reg = ins & ~( opcode );
					reg >>= 8;
					auto val = ( ( ( uint16_t )( ins << 8 ) ) >> 0xC ) & 0xFF; // lol 

					pC8->registers.ALL[ reg ] = ( rand( ) % 255 ) & val;
				}
			},

			{ // DXYN
				0xD000, FN
				{
				/* Display N-byte sprite starting at memory location I at (VX, VY). Each set bit of xored with what's already drawn.
				VF is set to 1 if a collision occurs. 0 otherwise.*/

				// immortalized to 4head at.
				//auto X = ins & ( 0x0D00 ); //Sorry I'm not bitwise pro
				//auto Y = ins & ( 0x00D0 );
				//auto height = ins & ( 0x000D );
				auto val = ins & ~( opcode );
				auto X = ( val & 0xF00 ) >> 8;
				auto Y = ( ( val & 0xF0 ) >> 4 ) & 0xF;
				auto height = ( val & 0xF );


				pC8->SetPixel( pC8->registers.ALL[ X ], pC8->registers.ALL[ Y ], height );
				}
			},

			{ // EX9E
				0xE09E, FN
				{ 
					// Skip next instruction if key in VX is pressed.
					auto reg = ( ins & ~(opcode) ) >> 8;
					auto key = pC8->registers.ALL[reg];
					if ( GetAsyncKeyState( pC8->KeyMap[key] ) & 1 )
						pC8->SetSkip( true );
				}
			},

			{ // EXA1
				0xE0A1, FN
				{  // Skip next instruction if key in VX !pressed.
					auto reg = ( ins & ~(opcode) ) >> 8;
					auto key = pC8->registers.ALL[reg];
					if ( !( GetAsyncKeyState( pC8->KeyMap[key] ) & 1) )
						pC8->SetSkip( true );
				}
			},

			{ // FX07
				0xF007, FN
				{
					// set vx to delay
					auto reg = ins & ~( opcode );
					reg >>= 8;
					pC8->registers.ALL[ reg ] = pC8->registers.delay;
				}
			},

			{ // FX0A
				0xF00A, FN
				{  
					//	A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)
					auto reg = ins & ~( opcode );
					reg >>= 8;
					int i = 0;

					while ( true )
					{
						for ( auto & a : pC8->KeyMap )
						{
							if ( GetAsyncKeyState( a.second ) & 1 )
							{
								pC8->registers.ALL[ reg ] = a.first;
								return;
							}
						}

						Sleep( 1 );
					}
				}
			},

			{ // FX15
				0xF015, FN
				{
					// set delay to vx
					auto reg = ins & ~( opcode );
					reg >>= 8;
					pC8->registers.delay = pC8->registers.ALL[ reg ];
				}
			},

			{ // FX18
				0xF018, FN
				{ // Sets the sound timer to VX.
					auto reg = ( ins & ~( opcode ) ) >> 8;
				pC8->registers.sound = pC8->registers.ALL[ reg ];
				}
			},

			{ // FX1E
				0xF01E, FN
				{ // add VX to I
					auto reg = ( ins & ~( opcode ) ) >> 8;
					pC8->registers.spritesheet += pC8->registers.ALL[ reg ];
				}
			},

			{ // FX29
				0xF029, FN
				{
					/* fuck happened here [ Axe happened ] huehue dota 2 maymays
					Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.\
					font shit will be stored in the upper 0x200 of memory*/

					auto reg = ins & ( 0x0F00 );
					reg >>= 8;
					pC8->registers.spritesheet = pC8->registers.ALL[ reg ] * 5; // EAAAAAASYYY
				}
			},

			{ //FX33
				0xF033, FN
				{ //Convert that word to BCD and store the 3 digits at memory location I through I+2. I does not change.
					auto val = ins & ~( opcode );
					auto X = ( val >> 8 );
					auto mem = pC8->GetMemory( );
					mem[ pC8->registers.spritesheet ] = X / 100;
					mem[ pC8->registers.spritesheet + 1 ] = ( X / 10 ) % 10;
					mem[ pC8->registers.spritesheet + 2 ] = ( X % 100 ) % 10;
					//Thanks JamesGriffin
				}
			},

			{ //FX55
				0xF055, FN
				{
					// Store registers V0 through VX in memory starting at location I. I does not change.
					auto X = ( ins >> 8 ) & 0xF;
					auto mem = pC8->GetMemory( );
					for ( auto i = 0; i <= X; i++ )
						mem[ pC8->registers.spritesheet + i ] = pC8->registers.ALL[ X ]; //Bad thing about this stuff is no proper checking for buffer overflow
				}
			},

			{ //FX65
				0xF065, FN
				{
					//Fills V0 to VX (including VX) with values from memory starting at address I. I is increased by 1 for each value written.
					//Fucking faggot increased it anyway kys
					auto X = ( ins >> 8 ) & 0xF;
					auto mem = pC8->GetMemory( );
					for ( auto i = 0; i <= X; i++ )
						pC8->registers.ALL[ i ] = mem[ pC8->registers.spritesheet + i];

				}
			}
		};
	}
}